import pytest
from selection_sort import selection_sort

def test_if_returns_list():
    assert isinstance(selection_sort([]), list)

def test_if_mutate_param():
    A = [3,2,1]
    assert selection_sort(A) == [1,2,3]

def test_if_sort_the_same_values():
    A = [1,1,1]
    assert selection_sort(A) == [1,1,1]

def test_if_sort_repeated_values():
    A = [11,11,33,33,66,66,44,44,44]
    assert selection_sort(A) == [11,11,33,33,44,44,44,66,66]

def test_if_sort_reverse_sorted_list():
    A = [5,4,3,2,1]
    assert selection_sort(A) == [1,2,3,4,5]

def test_if_sort_correctly():
    A = [1234,14,6,6,86,143,52,124,4,655,6,234,65134,513,463,564,562,456,2456,24,17,18,418,1,81,82,8,56,8,456,45,3,4,2,34,23,42,6,57,568,68,68,78,65,64,343,425,6,68,9,3442,34,457976,52,342,21,3,12,31,32]
    B = [1,2,3,3,4,4,6,6,6,6,6,8,8,9,12,14,17,18,21,23,24,31,32,34,34,42,45,52,52,56,57,64,65,68,68,68,78,81,82,86,124,143,234,342,343,418,425,456,456,463,513,562,564,568,655,1234,2456,3442,65134,457976]
    assert selection_sort(A) == B