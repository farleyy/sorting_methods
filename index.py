from sorting_methods.bubble_sort import bubble_sort
from sorting_methods.insertion_sort import insertion_sort
from sorting_methods.merge_sort import merge_sort
from sorting_methods.quick_sort import quick_sort
from sorting_methods.selection_sort import selection_sort

A = [3,2,1]

bubble_sort(A)
insertion_sort(A)
merge_sort(A)
quick_sort(A, 0, 2)
selection_sort(A)

